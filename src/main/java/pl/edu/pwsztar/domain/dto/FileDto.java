package pl.edu.pwsztar.domain.dto;

import org.springframework.core.io.InputStreamResource;

public class FileDto {

    private Long contentLength;
    private InputStreamResource inputStreamResource;
    private String fileName;

    public FileDto(){
    }

    private FileDto(Builder builder) {
        this.contentLength = builder.contentLength;
        this.inputStreamResource = builder.inputStreamResource;
        this.fileName = builder.fileName;
    }

    public InputStreamResource getInputStreamResource() {
        return inputStreamResource;
    }

    public long getContentLength() {
        return contentLength;
    }

    public String getFileName() {
        return fileName;
    }

    public static final class Builder {
        private Long contentLength;
        private InputStreamResource inputStreamResource;
        private String fileName;

        public Builder() {
        }

        public Builder contentLength(Long contentLength) {
            this.contentLength = contentLength;
            return this;
        }

        public Builder inputStreamResource(InputStreamResource inputStreamResource) {
            this.inputStreamResource = inputStreamResource;
            return this;
        }

        public Builder fileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public FileDto build() {
            return new FileDto(this);
        }

    }
}

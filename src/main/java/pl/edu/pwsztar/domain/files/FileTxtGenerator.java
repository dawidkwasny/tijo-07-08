package pl.edu.pwsztar.domain.files;

import pl.edu.pwsztar.domain.dto.MovieDto;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface FileTxtGenerator {
    File toTxt(List<MovieDto> movies) throws IOException;
}

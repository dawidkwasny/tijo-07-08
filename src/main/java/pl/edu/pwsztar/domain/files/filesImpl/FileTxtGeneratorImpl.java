package pl.edu.pwsztar.domain.files.filesImpl;

import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.files.FileTxtGenerator;

import java.io.*;
import java.util.List;

@Service
public class FileTxtGeneratorImpl implements FileTxtGenerator {

    @Override
    public File toTxt(List<MovieDto> movies) throws IOException {
        File file = File.createTempFile("tmp",".txt");
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
        BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);

        movies.forEach(movie -> writeMovie(bufferedWriter,movie));
        bufferedWriter.close();
        fileOutputStream.flush();
        fileOutputStream.close();

        return file;
    }
    private void writeMovie(BufferedWriter bufferedWriter, MovieDto movie) {
        try {
            bufferedWriter.write(movie.getYear()+" "+movie.getTitle());
            bufferedWriter.newLine();
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

}
